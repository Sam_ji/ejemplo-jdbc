import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.directory.ModificationItem;

import org.postgresql.ssl.DbKeyStoreSocketFactory.DbKeyStoreSocketException;

public class pbdJdbc {

	Connection conexion = null;

	public boolean dbConectar() {

		System.out.println("---dbConectar---");

		String driver = "org.postgresql.Driver";
		String servidor = "localhost"; // Direccion IP
		String puerto = "5432";
		String database = "Rol";
		String url = "jdbc:postgresql://" + servidor + ":" + puerto + "/" + database;
		String user = "postgres";
		String password = "12345";

		try {

			System.out.println("---Conectando a PostgreSQL---");
			Class.forName(driver);
			conexion = DriverManager.getConnection(url, user, password);
			System.out.println("Conexi�n realizada a la base de datos");
			return true;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/* ------------------------------------------------------------------ */

	static String readEntry(String prompt) {
		try {
			StringBuffer buffer = new StringBuffer();
			System.out.print(prompt);
			System.out.flush();
			int c = System.in.read();
			while (c != '\n' && c != -1) {
				buffer.append((char) c);
				c = System.in.read();
			}
			return buffer.toString().trim();
		} catch (IOException e) {
			return "";
		}
	}

	public boolean dbDesconectar() {
		System.out.println("---dbDesconectar---");

		try {
			// connection.commit();
			conexion.close();
			System.out.println("Desconexi�n realizada correctamente");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void dbObtenerEquipamientoJugadores() {
		Statement ps;
		String consulta;

		System.out.println("---Obteniendo equipamiento de jugadores---");

		try {
			ps = conexion.createStatement();
			consulta = "select jugador.nombre, arma.nombre, armadura.nombre from jugador JOIN arma ON (jugador.arma_id = arma.id) JOIN armadura ON (jugador.armadura_id = armadura.id)";
	
			ResultSet rset = ps.executeQuery(consulta);

			while (rset.next()) {
				System.out.println("-----------------------------------------");
				System.out.println("Nombre del jugador: " + rset.getString(1));
				System.out.println("Arma: " + rset.getString(2));
				System.out.println("Armadura: " + rset.getString(3));
				System.out.println("-----------------------------------------");

			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void dbObtenerArmas() {
		Statement ps;
		String consulta;

		System.out.println("---Obteniendo todas las armas---");

		try {
			ps = conexion.createStatement();
			consulta = "SELECT id, nombre, danio, bonus_fuerza, bonus_destreza, bonus_inteligencia FROM arma";
	
			ResultSet rset = ps.executeQuery(consulta);

			while (rset.next()) {
				System.out.println("-----------------------------------------");
				System.out.println("ID del arma: " + rset.getInt(1));
				System.out.println("Nombre del arma: " + rset.getString(2));
				System.out.println("Da�o del arma: " + rset.getInt(3));
				System.out.println("Bonus fuerza: " + rset.getInt(4));
				System.out.println("Bonus destreza: " + rset.getInt(5));
				System.out.println("Bonus inteligencia: " + rset.getInt(6));
				System.out.println("-----------------------------------------");

			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void dbInsertarArma() {
		Statement ps;
		String consulta;
		String nombreArma, danio;
		int resultado;

		System.out.println("---Insertando nueva arma---");

		try {
			ps = conexion.createStatement();

			nombreArma = readEntry("Nombre del arma: ");// por ejemplo
																// VENTAS
			danio = readEntry("Da�o del arma: ");

			consulta = "INSERT INTO ARMA VALUES (5,'" + nombreArma + "','" + danio+ "',0,0,0)";

			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* ------------------------------------------------------------------ */

	public void dbModificarArmadura() {
		Statement ps;
		String consulta;
		String nombre_n;
		int resultado;

		System.out.println("---Modificar la armadura con ID 1 (Originalmente \"Cilantro armor\"---");

		try {
			ps = conexion.createStatement();

			nombre_n = "Camiseta Hawaiana";

			consulta = "UPDATE ARMADURA SET NOMBRE = '" + nombre_n + "' WHERE ID=1";

			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void dbRevertModificarArmadura() {
		Statement ps;
		String consulta;
		String nombre_n;
		int resultado;

		System.out.println("---Revertir cambios en Cilantro Armor---");

		try {
			ps = conexion.createStatement();

			nombre_n = "Cilantro Armor";

			consulta = "UPDATE ARMADURA SET NOMBRE = '" + nombre_n + "' WHERE ID=1";

			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* ------------------------------------------------------------------ */

	public void dbBorrarArma() {
		Statement ps;
		String consulta;
		int resultado, numeroArma;

		System.out.println("---Borrar arma con id = 5 (Creada anteriormente)---");

		try {
			ps = conexion.createStatement();

			numeroArma = 5;

			consulta = "DELETE FROM ARMA WHERE id = '" + numeroArma + "'";

			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] argv) {
		pbdJdbc cliente = new pbdJdbc();
		System.out.println("---Programa principal---");

		if (!cliente.dbConectar()) {
			System.out.println("Conexi�n no realizada");
		}
		
		cliente.dbObtenerEquipamientoJugadores();
		cliente.dbInsertarArma();
		cliente.dbObtenerArmas();
		cliente.dbModificarArmadura();
		cliente.dbObtenerEquipamientoJugadores();
		cliente.dbBorrarArma();
		cliente.dbObtenerArmas();
		cliente.dbRevertModificarArmadura();
		cliente.dbObtenerEquipamientoJugadores();
		
		if (!cliente.dbDesconectar()) {
			System.out.println("Desconexi�n no realizada");
		}
		System.out.println("---Fin de programa---");

	}

}