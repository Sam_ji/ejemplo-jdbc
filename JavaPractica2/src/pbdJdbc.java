import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.directory.ModificationItem;

import org.postgresql.ssl.DbKeyStoreSocketFactory.DbKeyStoreSocketException;

public class pbdJdbc {

	Connection conexion = null;

	public boolean dbConectar() {
		
		System.out.println("---dbConectar---");
		
		String driver = "org.postgresql.Driver";
		String servidor = "localhost"; // Direccion IP
		String puerto = "5432";
		String database = "Empresa";
		String url = "jdbc:postgresql://" + servidor + ":" + puerto + "/"
				+ database;
		String user = "postgres";
		String password = "12345";

		try {

			System.out.println("---Conectando a PostgreSQL---");
			Class.forName(driver);
			conexion = DriverManager.getConnection(url, user, password);
			System.out.println("Conexión realizada a la base de datos");
			return true;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

/* ------------------------------------------------------------------ */
		
	static String readEntry(String prompt) {
		try {
			StringBuffer buffer = new StringBuffer();
			System.out.print(prompt);
			System.out.flush();
			int c = System.in.read();
			while (c != '\n' && c != -1) {
				buffer.append((char) c);
				c = System.in.read();
			}
			return buffer.toString().trim();
		} catch (IOException e) {
			return "";
		}
	}

/* ------------------------------------------------------------------ */

	public void dbObtenerEmpleado1() {
		PreparedStatement ps;
		String consulta;

		System.out.println("---dbObtenerEmpleado1---");

		try {
			ps = conexion
					.prepareStatement("SELECT NOMBRE, APELLIDO1, APELLIDO2, DIRECCION, SUELDO FROM EMPLEADO WHERE DNI = ?");

			// Por ejemplo, buscar el empleado con DNI 987654321
			consulta = readEntry("Introduce DNI del empleado: ");
			ps.clearParameters();
			ps.setString(1, consulta);
			ResultSet rset = ps.executeQuery();

			while (rset.next()) {
				System.out.println("Nombre: " + rset.getString(1));
				System.out.println("Apellidos: " + rset.getString(2) + " "
						+ rset.getString(3));
				System.out.println("Direccion: " + rset.getString(4));
				System.out.println("Sueldo: " + rset.getFloat(5));
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

/* ------------------------------------------------------------------ */
	
	public void dbObtenerEmpleado2() {
		Statement ps;
		String consulta;

		System.out.println("---dbObtenerEmpleado2---");

		try {
			ps = conexion.createStatement();

			// Por ejemplo, buscar el empleado con DNI 987654321
			consulta = readEntry("Introduce DNI del empleado: ");
			String result = "SELECT NOMBRE, APELLIDO1, APELLIDO2, DIRECCION, SUELDO FROM EMPLEADO WHERE DNI = "
					+ "'" + consulta + "'";
			ResultSet rset = ps.executeQuery(result);

			System.out.println(result);

			if (rset.next()) {
				System.out.println("Nombre: " + rset.getString(1));
				System.out.println("Apellidos: " + rset.getString(2) + " "
						+ rset.getString(3));
				System.out.println("Direccion: " + rset.getString(4));
				System.out.println("Sueldo: " + rset.getFloat(5));
				rset.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


/* ------------------------------------------------------------------ */
	
	public void dbConsultarEmpleado() {
		Statement ps;
		String consulta;

		System.out.println("---dbConsultarEmpleado---");

		try {
			ps = conexion.createStatement();

			consulta = "SELECT NOMBRE, APELLIDO1, APELLIDO2, DNI, FECHANAC, DIRECCION, SEXO, SUELDO, SUPERDNI, DNO FROM EMPLEADO";
			ResultSet rset = ps.executeQuery(consulta);

			System.out.println(consulta);

			while (rset.next()) {
				System.out.println("Nombre: " + rset.getString(1));
				System.out.println("Apellido 1: " + rset.getString(2));
				System.out.println("Apellido 2: " + rset.getString(3));
				System.out.println("DNI: " + rset.getString(4));
				System.out.println("Fecha Nacimiento: " + rset.getString(5));
				System.out.println("Direccion: " + rset.getString(6));
				System.out.println("Sexo: " + rset.getString(7));
				System.out.println("Sueldo: " + rset.getString(8));
				System.out.println("DNI Supervisor: " + rset.getString(9));
				System.out.println("DNO: " + rset.getInt(10));
				System.out.println("--------------------------------------");
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

/* ------------------------------------------------------------------ */	

	public boolean dbDesconectar() {
		System.out.println("---dbDesconectar---");

		try {
			// connection.commit();
			conexion.close();
			System.out.println("Desconexión realizada correctamente");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

/* ------------------------------------------------------------------ */
	
	public void dbConsultarDepartamento() {
		Statement ps;
		String consulta;

		System.out.println("---dbConsultarDepartamento---");

		try {
			ps = conexion.createStatement();

			consulta = "SELECT NOMBREDPTO, NUMERODPTO, DNIDIRECTOR, FECHAINGRESODIRECTOR FROM DEPARTAMENTO";
			ResultSet rset = ps.executeQuery(consulta);

			System.out.println(consulta);

			while (rset.next()) {
				System.out.println("NombreDpto: " + rset.getString(1));
				System.out.println("NumeroDpto: " + rset.getString(2));
				System.out.println("DniDirector: " + rset.getString(3));
				System.out.println("FechaIngresoDirector: " + rset.getString(4));
				System.out.println("--------------------------------------");
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

/* ------------------------------------------------------------------ */
	
	public void dbInsertarDepartamento() {
		Statement ps;
		String consulta;
		String nombreD, numeroD, dniDir, fIngresoDir;
		int resultado;
		
		System.out.println("---dbInsertarDepartamento---");
		
		try {
			ps = conexion.createStatement();
					
			nombreD = readEntry("Nombre del departamento: ");// por ejemplo VENTAS
			numeroD = readEntry("Numero del departamento: ");
			dniDir = readEntry("DNI del director: ");
			fIngresoDir = readEntry("Fecha de ingreso del director: ");
			consulta ="INSERT INTO DEPARTAMENTO VALUES ('"+nombreD+"','"+numeroD+"','"+dniDir+"','"+fIngresoDir+"')";

			resultado = ps.executeUpdate(consulta);
			
			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: "+resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

/* ------------------------------------------------------------------ */	
	
	public void dbModificarDepartamento() {
		Statement ps;
		String consulta;
		String nombreD;
		int resultado;

		System.out.println("---dbModificarDepartamento---");

		try {
			ps = conexion.createStatement();

			nombreD = "COMPRAS";

			consulta = "UPDATE DEPARTAMENTO SET NOMBREDPTO = '"+nombreD+"' WHERE NUMERODPTO=6";
			
			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

/* ------------------------------------------------------------------ */
	
	public void dbBorrarDepartamento() {
		Statement ps;
		String consulta;
		int resultado, numeroD;

		System.out.println("---dbBorrarDepartamento---");

		try {
			ps = conexion.createStatement();

			numeroD = 6;// Borrar el departamento 6

			consulta ="DELETE FROM DEPARTAMENTO WHERE NUMERODPTO = '"+numeroD+"'";
			
			resultado = ps.executeUpdate(consulta);

			System.out.println(consulta);
			System.out.println("Numero de filas afectadas: " + resultado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

/* ------------------------------------------------------------------ */
	
	public void dbExamen(){
		Statement ps;
		String consulta;

		System.out.println("---dbConsultarDepartamento---");

		try {
			ps = conexion.createStatement();

			consulta = "SELECT departamento.nombredpto, empleado.nombre, empleado.apellido1, empleado.apellido2 FROM departamento JOIN empleado ON (departamento.dnidirector = empleado.dni)";
			ResultSet rset = ps.executeQuery(consulta);

			System.out.println(consulta);

			while (rset.next()) {
				System.out.println("NombreDpto: " + rset.getString(1));
				System.out.println("Nombre Director: " + rset.getString(2));
				System.out.println("Apellido1 Director: " + rset.getString(3));
				System.out.println("Apellido2 Director: " + rset.getString(4));
				System.out.println("--------------------------------------");
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

/* ------------------------------------------------------------------ */
	
	public static void main(String[] argv) {
		pbdJdbc cliente = new pbdJdbc();
		System.out.println("---Programa principal---");

		if (!cliente.dbConectar()) {
			System.out.println("Conexión no realizada");
		}
		
//		cliente.dbExamen();
		cliente.dbObtenerEmpleado1();
		cliente.dbObtenerEmpleado2();
		
		cliente.dbConsultarEmpleado();
		cliente.dbConsultarDepartamento();
		
		cliente.dbInsertarDepartamento();
		cliente.dbConsultarDepartamento();
		
		cliente.dbModificarDepartamento();
		cliente.dbConsultarDepartamento();
		
		cliente.dbBorrarDepartamento();
		cliente.dbConsultarDepartamento();

		if (!cliente.dbDesconectar()) {
			System.out.println("Desconexión no realizada");
		}
		System.out.println("---Fin de programa---");

	}

}